﻿namespace csMaps
{
    internal class MapsCalc
    {
        internal const double PIx    = 3.141592653589793;
        internal const double RADIUS = 6378.16;

        //Returns calculated Radians:
        internal static double Radians(double x) => x * PIx / 180;
    }
}